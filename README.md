This repository is a blog type cool website created using latest web technologies such as HTML5, JavaScript and Classic ASP.
The main aim of this project was to achieve the functionalities of any blog using Classic ASP. Hence all the features of Classic ASP must be used during the implementation phase of this project.

Contributors:
Raj Parekh: Design, Development.

The current version of the repository is the final iteration of the project and it won't be updated or contributed by anyone.
Anything in this repository is 100% percent reusable as per the requirements.

Status: 100% finished.